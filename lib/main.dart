/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'bottom_nav_bar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Test',
      theme: ThemeData(
        fontFamily: 'Montserrat',
      ),
      home: BottomNavBar(),
    );
  }
}
