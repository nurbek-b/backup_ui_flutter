/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

/* Local dependencies */
import 'screens/settings_screen/settings_screen.dart';
import 'screens/backup_screen/backup_screen.dart';
import 'screens/history_screen/history_screen.dart';
import 'widgets/nested_navigator.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int currentIndex = 0;

  PageController _pageController = PageController(initialPage: 0);

  final GlobalKey<NavigatorState> navigationKey = GlobalKey<NavigatorState>();

  PageView pageViewBuilder(ctx) => PageView(
        controller: _pageController,
        onPageChanged: (val) {
          setState(() {
            currentIndex = val;
          });
        },
        children: <Widget>[HistoryScreen(), BackupScreen(), SettingsScreen()],
      );

  /// Build BottomNavigationBar Widget
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Colors.blueGrey,
        showSelectedLabels: true,
        selectedIconTheme: IconThemeData(color: Color(0xFF00AB50)),
        currentIndex: currentIndex,
        onTap: (val) {
          setState(() {
            currentIndex = val;
            while (navigationKey.currentState.canPop()) {
              navigationKey.currentState.pop();
            }
            _pageController.animateToPage(val,
                duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
          });
        },
        items: [
          BottomNavigationBarItem(
              label: '',
              icon: Image.asset(
                'assets/icons/history_icon.png',
                height: screenSize.height * 0.05,
              ),
              activeIcon: Image.asset(
                'assets/icons/active_history_icon.png',
                height: screenSize.height * 0.05,
              )),
          BottomNavigationBarItem(
              label: '',
              icon: Image.asset(
                'assets/icons/backup_icon.png',
                height: screenSize.height * 0.05,
              ),
              activeIcon: Image.asset(
                'assets/icons/active_backup_icon.png',
                height: screenSize.height * 0.05,
              )),
          BottomNavigationBarItem(
              label: '',
              icon: Image.asset(
                'assets/icons/settings_icon.png',
                height: screenSize.height * 0.05,
              ),
              activeIcon: Image.asset(
                'assets/icons/active_settings_icon.png',
                height: screenSize.height * 0.05,
              )),
        ],
      ),
      body: NestedNavigator(
        navigationKey: navigationKey,
        initialRoute: '/',
        routes: {'/': pageViewBuilder},
      ),
    );
  }
}
