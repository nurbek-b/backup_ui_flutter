/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'components/body.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Body(),
      ),
    );
  }
}
