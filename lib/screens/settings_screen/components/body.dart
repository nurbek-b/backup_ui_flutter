/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import '../../../screens/screen_widgets/screen_title.dart';
import 'curve_paint.dart';
import 'contacts_item.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          children: [
            PageTitle(
              title: 'Settings',
            ),
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Container(
                  child: Center(
                    child: CustomPaint(
                      size: Size(500, 500),
                      painter: CurvePainter(),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: [
                      ContactItem(title: 'Help'),
                      ContactItem(title: 'Privacy Policy'),
                      ContactItem(title: 'Terms of Use'),
                      ContactItem(title: 'Support'),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
