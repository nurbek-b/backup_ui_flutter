/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */

class ContactItem extends StatelessWidget {
  final String title;

  const ContactItem({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.03),
        child: Text(
          title,
          style: TextStyle(
            fontSize: 20.0,
            color: Color(0xFF6465F9),
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
