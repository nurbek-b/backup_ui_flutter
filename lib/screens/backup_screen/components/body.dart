/* External dependencies */
import 'dart:ui';
import 'package:contact_backup_app/widgets/my_page_router.dart';
import 'package:flutter/material.dart';

/* Local dependencies */
import '../../../screens/screen_widgets/screen_title.dart';
import 'complete_page.dart';
import 'curve_paint.dart';

class Body extends StatefulWidget {
  const Body({Key key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String title;
  String description;
  String toAction;
  LinearGradient gradient;
  double internalRadius;
  double externalRadius;
  Color color1;
  Color color2;
  Color circleColor;
  bool showButton;

  @override
  void initState() {
    title = 'Contacts Backup';
    description =
        'Backup your address book to avoid losing your contacts in case something happens to your phone.';
    toAction = 'Create \nBackup';
    internalRadius = 246.0;
    externalRadius = 300.0;
    color1 = Color(0xFFF3F3F3);
    color2 = Color(0xFFF3F3F3);
    circleColor = Color(0xFF56F3C4);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          PageTitle(
            title: title,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
            child: Text(
              description,
              textAlign: TextAlign.center,
              maxLines: 3,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            alignment: Alignment.topCenter,
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                CustomPaint(
                  size: Size(500, 500),
                  painter: CurvePainter(color1: color1, color2: color2),
                ),
                Container(
                  width: externalRadius,
                  height: externalRadius,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      title = 'Backing Up..';
                      description =
                          'Please sit tight while your contacts are being backed up.';
                      toAction = '25%';
                      internalRadius = 196.0;
                      externalRadius = 246.0;
                      color1 = Color(0xFF4D4AF9);
                      color2 = Color(0xFF56F3C4);
                      circleColor = Color(0xFF6465F9);
                    });
                    Future.delayed(const Duration(seconds: 3), () {
                      setState(() {
                        toAction = '50%';
                        internalRadius = 224.0;
                        externalRadius = 278.0;
                      });
                    });
                    Future.delayed(const Duration(seconds: 6), () {
                      setState(() {
                        toAction = '75%';
                        internalRadius = 278.0;
                        externalRadius = 327.0;
                      });
                    });
                    Future.delayed(const Duration(seconds: 9), () {
                      setState(() {
                        Navigator.of(context)
                            .push(CustomPageRoute(CompleteScreen()));
                      });
                    });
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: internalRadius,
                    height: internalRadius,
                    decoration: new BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xFF4D4AF9),
                          circleColor,
                        ],
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      toAction,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: 30.0),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
