/* External dependencies */
import 'package:flutter/material.dart';

class BackupButton extends StatelessWidget {
  final String text;

  const BackupButton({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      child: Text(
        'Open Backup',
        style: TextStyle(color: Color(0xFF6465F9), fontWeight: FontWeight.w900),
      ),
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    );
  }
}
