/* External dependencies */
import 'package:flutter/material.dart';

class CurvePainter extends CustomPainter {
  Color color1;
  Color color2;

  CurvePainter({this.color1, this.color2});

  @override
  void paint(Canvas canvas, Size size) {
    var rect = Offset.zero & size;
    var paint = Paint();
    paint.shader = LinearGradient(
      begin: Alignment.center,
      end: Alignment.bottomRight,
      colors: [
        color1,
        color2,
      ],
    ).createShader(rect);
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, size.height * 0.5);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.38,
        size.width * 0.5, size.height * 0.5);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.68,
        size.width * 1.0, size.height * 0.5);
    path.lineTo(size.width, size.height * 2);
    path.lineTo(0, size.height * 2);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
