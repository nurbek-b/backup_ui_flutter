/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'backup_button.dart';

class CompleteScreen extends StatelessWidget {
  const CompleteScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: double.infinity,
      decoration: new BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomRight,
          stops: [0.3, 1.3],
          colors: [Color(0xFF4D4AF9), Color(0xFF56F3C4)],
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: screenSize.height * 0.1),
            child: Text(
              'Completed!',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Colors.white),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: screenSize.width * 0.05,
                right: screenSize.width * 0.05,
                top: screenSize.width * 0.1),
            child: Text(
              'Your backup has been completed and saved to your archive.',
              textAlign: TextAlign.center,
              maxLines: 3,
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: screenSize.height * 0.24),
            child: Text(
              '100%',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50,
                  color: Colors.white),
            ),
          ),
          SizedBox(
            height: screenSize.height * 0.06, //height of button
            width: screenSize.width * 0.9,
            child: BackupButton(),
          )
        ],
      ),
    );
  }
}
