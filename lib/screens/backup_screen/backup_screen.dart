/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'components/body.dart';

class BackupScreen extends StatelessWidget {
  const BackupScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Body();
  }
}
