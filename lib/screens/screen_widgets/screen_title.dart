/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */

class PageTitle extends StatelessWidget {
  final String title;

  const PageTitle({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: screenSize.height * 0.06),
        child: Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 30, color: Colors.black),
        ),
      ),
    );
  }
}
