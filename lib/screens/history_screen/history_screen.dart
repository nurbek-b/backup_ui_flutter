/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'components/body.dart';

class HistoryScreen extends StatelessWidget {
  const HistoryScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Body(),
    );
  }
}
