/* External dependencies */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

/* Local dependencies */
import '../../../screens/screen_widgets/screen_title.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return SafeArea(
      child: Column(
        children: [
          PageTitle(
            title: 'History',
          ),
          SizedBox(
            height: screenSize.height * 0.02,
          ),
          Divider(
            color: Colors.grey,
          ),
          ListView.separated(
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return ListTile(
                    leading: Image.asset('assets/icons/cloud_icon.png',
                        height: screenSize.height * 0.03),
                    title: Text(
                      'May 18, 2021',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text('07:54pm · 2 contacts · 124 Kb'),
                    trailing: InkWell(
                      onTap: () {
                        Share.shareFiles(
                          ['May 18, 2021'],
                          text: 'This is test',
                          subject: 'Subject test',
                        );
                      },
                      child: Icon(
                        CupertinoIcons.share,
                        color: Color(0xFF6465F9),
                      ),
                    ));
              },
              separatorBuilder: (context, index) => Divider(
                    color: Colors.grey,
                  ),
              itemCount: 2),
        ],
      ),
    );
  }
}
